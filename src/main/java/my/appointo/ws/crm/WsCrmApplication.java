package my.appointo.ws.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsCrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsCrmApplication.class, args);
	}

}
